<?php

return array(
    /*
     * Общие настройки 
     */
    'siteDir' => dirname(__DIR__),
    'siteUrl' => 'http://autotadam.ru/_test',
    /*
     * Настройки БД 
     */
    'dbParams' => [
        'dbHost' => 'localhost',
        'dbUser' => 'root',
        'dbPass' => '',
        'dbName' => '_test',
        'dbPrefix' => '',
        'dbType' => 'mysqli', //mysqli || pdo || sqllite3
    ],
    /*
     * Доступные библиотеки для работы с БД
     */
    'possibleDb' => [
        'mysqli' => 'natCMF\core\sql\DbMysqli',
    ],
    
    //Тип сессий
    
    'session_type' => 'native',
    
    
    //Дефолтный модуль, дефолтный контроллер имеет такое же имя как и модуль
    //дефолтный экшен всегда index
    'defaultModule' => 'front',
    
    //Меню в админке
    'adminNavBar' => [
        '/cabinet' => 'Главная',
        '/cabinet/add-column' => 'Добавить столбец',
        '/cabinet/add-row' => 'Добавить строку',
        '/user/logout' => 'Выход',
    ],
);
