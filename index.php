<?php

$mem_start = memory_get_usage();

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/*
 * Дебаг информация
 */
ini_set('display_errors', 1);
error_reporting(E_ALL);

/* @var $settingsCore array */
$settingsApp = require __DIR__ . '/config/app.php';

/**
 * Автозагрузка классов приложения
 * @param type $classname
 */
function oneLoad($classname) {
    
    $path = str_replace('natCMF\\', '', $classname);
    $file = __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $path) . '.php';
    if (file_exists($file)) {
        include $file;
    }
}

spl_autoload_register('oneLoad');


/*
 * Инициализируем приложение
 */
$app = new natCMF\core\App($settingsApp);
$app->run();



//Инфа о выполнении
echo '<hr style="margin-bottom: 0px;" />Запросов: ' . \natCMF\core\Db::getCountQuery() . '<br />Занято памяти: ' . round(((memory_get_usage() - $mem_start) / 1024 / 1024), 3) . ' Мб.';
