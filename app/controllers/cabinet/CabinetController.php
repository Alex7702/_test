<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\app\controllers\cabinet;

use natCMF\core\ControllerBase;
use natCMF\core\View;
use natCMF\core\Config;
use natCMF\app\models\User;
use natCMF\core\App;

/**
 * Description of CabinetController
 *
 * @author 27087
 */
class CabinetController extends ControllerBase {

    public function actionIndex() {
        
        //Взяли данные из модели 
        $user = new User;
        $table = $user->buildTableData();
        
        //Создали вьюху и отправили туда данные
        $view = new View('cabinet');
        $view->render('cabinet-index', [
            'table' => $table,
            'pageId' => 'index',
            'adminMenuItems' => Config::get('adminNavBar'),
        ]);
        
    }
    
    /**
     * Удадить строку
     * @param type $id
     */
    public function actionDeleteRow($id) {
        $user = new User;
        $user->deleteById($id);
        App::redirect('/cabinet');
    }
    
    public function actionDropColumn($name) {
        $user = new User;
        
        if(isset($user->tableCollumns[$name])
            && !in_array($name, $user->fixCollumns)){
            $user->dropCollumn($name);       
        }
        
        App::redirect('/cabinet');
        
    }
    
    public function actionAddRow() {
        $user = new User;
        $post = filter_input_array(INPUT_POST);
        $error = [];
        if(!is_null($post)){
            $user->addRow($post);
            App::redirect('/cabinet');
        }
        
        //Создали вьюху и отправили туда данные
        $view = new View('cabinet');
        $view->render('cabinet-add-row', [
            'columns' => $user->tableCollumns,
            'error' => $error,
            'pageId' => 'cabinet-add-row',
            'adminMenuItems' => Config::get('adminNavBar'),
        ]);
        
    }
    
    /**
     * Добавить новую колонку
     */
    public function actionAddColumn() {
        
        $user = new User;
        $post = filter_input_array(INPUT_POST);
        
        $error = [];
        if(!is_null($post)){
            if(empty($post['name'])){
                $error[] = 'Имя не может быть пустое';
            }
            if(empty($post['type'])){
                $error[] = 'Тип не может быть пустой';
            }
            if(empty($error)){
                $user->addCollumn($post['name'], $post['type']);
                App::redirect('/cabinet');
            }
        }
        $name = $post['name'] ?? '';
        $type = $post['type'] ?? '';
        

        
        //Создали вьюху и отправили туда данные
        $view = new View('cabinet');
        $view->render('cabinet-add-column', [
            'error' => $error,
            'name' => $name,
            'type' => $type,
            'pageId' => 'index',
            'adminMenuItems' => Config::get('adminNavBar'),
        ]);
    }

}
