<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\app\models;

use natCMF\core\SmartModel;
use natCMF\core\Db;
use natCMF\core\App;

/**
 * Description of User
 *
 * @author 27087
 */
class User extends SmartModel {

    public $tableName = 'users';
    public $fixCollumns = ['id', 'name'];

    /**
     * Собраем массив для вывода в таблицу
     * @return array
     */
    public function buildTableData() {

        //Инициализируем массив таблицы
        $table = [];

        //Добавляем в него данные таблицы
        $table['data'] = $this->getRows('
            SELECT *
            FROM {db_prefix}users');

        //Заголовок таблицы
        $table['title'] = 'Моя таблица';

        //Собрали колонки для хелпера
        foreach ($this->tableCollumns as $name => $value) {
            $table['columns'][$name] = [
                'title' => $name . ' ' .
                (in_array($name, $this->fixCollumns) ? '' : '<a class="drop-column" href="' . App::href('/cabinet/drop-column', [urlencode($name)]) . '" title="Удалить">X</a>'),
                'value' => $value
            ];
        }
        $table['columns']['oper1231231'] = [
            'title' => 'Операции',
            'value' => function($v){
                $s = '<a class="remove-row" href="'.App::href('/cabinet/delete-row', [$v['id']]).'">Удалить</a>';
                return $s;
            }

        ];

        return $table;
    }

    /**
     * Добавляем столбец в базу
     * @param string $name - имя столбца
     * @param string $type - тип слобца int || varchar
     */
    public function addCollumn(string $name, string $type) {
        $name = str_replace(' ', '_', $name);
        switch ($type) {
            case 'varchar':
                $this->addVarcharColumn($name);
                break;

            case 'int':
                $this->addIntColumn($name);
                break;
        }
    }

    /**
     * Добавляем varchar столбец в базу
     * @param string $name - имя столбца
     */
    private function addVarcharColumn(string $name) {
        Db::query('
            ALTER TABLE {db_prefix}users 
            ADD `' . $name . '` 
                VARCHAR(255) 
                CHARACTER SET utf8mb4 
                COLLATE utf8mb4_unicode_ci 
                NOT NULL 
                DEFAULT \'\' 
            ');
    }

    /**
     * Добавляем int столбец в базу
     * @param string $name - имя столбца
     */
    private function addIntColumn(string $name) {
        Db::query('
            ALTER TABLE {db_prefix}users 
            ADD ' . $name . ' 
                INT(10)
                NOT NULL 
                DEFAULT `0`
            ');
    }
    
    public function addRow($data) {
        $this->insertRow($data);
    }
    

    public function dropCollumn($name) {
        Db::query('
            ALTER TABLE {db_prefix}users 
            DROP `'.$name.'`
            ');
        
    }
    

}
