<?php

use natCMF\core\Config;
use natCMF\core\App;
use natCMF\core\helpers\Html;
?>

<script>
    $(function () {
        $('.drop-column').on('click', function () {
            return confirm("Удалить столбец?");
        });
    });
</script>

<?= $this->layout('navbar') ?>
<div class="container-fluid">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-md-12">
            <h2>админка</h2>
            <div class="content">

                <form action="<?= App::href('') ?>" method="POST">
                    <?php
                    foreach ($columns as $name => $param) {
                        if ($name == 'id') {
                            continue;
                        }
                        echo '
                            ' . $name . '(' . $param['type'] . ') <input type="string" name="' . $name . '" /><br />';
                    }
                    ?>
                    <input type="submit" />
                </form>

            </div><!--/content-->
        </div><!--/.col-xs-12.col-sm-9-->
    </div><!--/row-->
</div><!--/.container-->

