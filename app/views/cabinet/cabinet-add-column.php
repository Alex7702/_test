<?php

use natCMF\core\Config;
use natCMF\core\App;
use natCMF\core\helpers\Html;

//console.log('test');        
$js = <<< JS
JS;
$css = <<< CSS
console.log('test');        
CSS;

$this->addJs($js);
?>

<?= $this->layout('navbar') ?>
<div class="container-fluid">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-md-12">
            <h2>админка</h2>
            <div class="content">
                <?php
                    if(!empty($error)){
                        echo '<div class="alert alert-warning">' .implode('/n/r', $error) . '</div>';
                    }
                ?>
                <form action="<?= App::href('') ?>" method="POST">
                    Имя столбца: <input name="name" type="text" size="40" value="<?= $name ?>"/><br />
                    Тип столбца: <select name="type">
                        <option <?= $type=='int' ? 'selected' : '' ?> value="int">
                            int(10)
                        </option>
                        <option <?= $type=='varchar' ? 'selected' : '' ?> value="varchar">
                            varchar(255)
                        </option>
                    </select><br />
                    <input type="submit">
                </form>

            </div><!--/content-->
        </div><!--/.col-xs-12.col-sm-9-->
    </div><!--/row-->
</div><!--/.container-->

