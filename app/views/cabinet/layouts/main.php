<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?= $this->templateUrl ?>/image/favicon.ico">

        <title>Dashboard Template for Bootstrap</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Bootstrap core CSS -->
        <link href="<?= $this->templateUrl ?>/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?= $this->templateUrl ?>/css/dashboard.css" rel="stylesheet">

        <link rel="stylesheet" href="<?= $this->templateUrl ?>/vendors/font-awesome/css/font-awesome.min.css">

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="<?= $this->templateUrl ?>/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
            /*
             * Off Canvas
             * --------------------------------------------------
             */
            @media screen and (max-width: 767px) {
                .row-offcanvas {
                    position: relative;
                    -webkit-transition: all .25s ease-out;
                    -o-transition: all .25s ease-out;
                    transition: all .25s ease-out;
                }

                .sidebar-offcanvas {
                    position: absolute;
                    width: 60%; /* 6 columns */
                    display: none;
                }

                .row-offcanvas-right {
                    right: 0;
                }

                .row-offcanvas-right.active
                .sidebar-offcanvas {
                    display: block;  
                    right: 0;
                    top: 0;
                    z-index: 1;
                }

                .row-offcanvas-right.active {

                }

            }

            @media screen and (min-width: 767px) {
                .navbar-nav {
                    float: right;
                }

            }

            .navbar-inverse {
                background-color: #222;
                border-color: #080808;
            }

            .navbar-fixed-top {
                top: 0;
                border-width: 0 0 1px;
            }

            .right-fix {
                float: right!important;
                margin: 9px 10px;
            }

            #sidebar {
                padding: 9px 5px 5px 5px;
            }


            
        </style>

    </head>

    <body>




        <?= $this->action($action) ?>



        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?= $this->templateUrl ?>/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= $this->templateUrl ?>/js/docs.min.js"></script>
        <script src="<?= $this->templateUrl ?>/js/admin.js"></script>


        <script src="<?= $this->templateUrl ?>/vendors/bootstrap-notify/bootstrap-notify.min.js"></script>
        <script>
            var notify_timer = 1000;
            var notify_delay = 5000;
            $(function () {
                $.notifyDefaults({
                    // settings
                    element: 'body',
                    position: null,
                    allow_dismiss: true,
                    newest_on_top: false,
                    showProgressbar: false,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                    delay: notify_delay,
                    timer: notify_timer,
                    url_target: '_blank',
                    mouse_over: null,
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    },
                    onShow: null,
                    onShown: null,
                    onClose: null,
                    onClosed: null,
                    icon_type: 'class',
                    template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                            '<span data-notify="icon"></span> ' +
                            '<span data-notify="title">{1}</span> ' +
                            '<span data-notify="message">{2}</span>' +
                            '<div class="progress" data-notify="progressbar">' +
                            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                            '</div>' +
                            '<a href="{3}" target="{4}" data-notify="url"></a>' +
                            '</div>'
                });
<?php
if (!empty($error)) {
    foreach ($error as $message) {
        echo "
                notify_delay = notify_delay + 5000;
                $.notify({
                    icon: 'glyphicon glyphicon-warning-sign',
                    //title: 'Внимание',
                    message: '{$message['msg']}',
                    //url: 'https://github.com/mouse0270/bootstrap-notify',
                    //target: '_blank'
                },{
                    delay: notify_delay,
                    type: '{$message['type']}',
                });";
    }
}
?>

            });


        </script>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>




        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script>
            $(function () {
                $('.multi_sortable').sortable();
                $('.multi_sortable').disableSelection();

                $('.selectpicker').selectpicker({
                    style: 'btn-info',
                    size: 4
                });

                $('body').on('click', 'div.variable', function () {
                    var range, selection;
                    if (document.createRange) {
                        range = document.createRange();
                        range.selectNode(this);
                        selection = window.getSelection();
                        var strsel = '' + selection;
                        if (!strsel.length) {
                            selection.removeAllRanges();
                            selection.addRange(range);
                        }
                    } else { //В IE ниже 9 не будет контроля выделения части текста
                        var range = document.body.createTextRange();
                        range.moveToElementText(this);
                        range.select();
                    }
                });

            });

            $(document).ready(function () {
                $('[data-toggle="offcanvas"]').click(function () {
                    $('.row-offcanvas').toggleClass('active')
                });
            });



        </script>

    </body>
</html>