<?php

use natCMF\core\App;
use natCMF\core\Config;
use natCMF\core\Router;
?>
<nav class="navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">


            <button type="button" class="right-fix pull-left btn btn-default visible-xs" data-toggle="offcanvas" aria-expanded="false" aria-controls="navbar">
                <i class="fa fa-navicon"></i>
            </button>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= App::href('/') ?>">На сайт</a>
        </div>

        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?php
                foreach ($adminMenuItems as $key => $value):
                    $active = '/' . Router::uri() == $key ? 'active' : '';
                    ?>
                    <li class="<?= $active ?>"><a href="<?= App::href($key) ?>"><?= $value ?></a></li>                    
<?php endforeach; ?>
            </ul>
        </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
</nav><!-- /.navbar -->
