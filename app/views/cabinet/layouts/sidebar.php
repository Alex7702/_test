<?php
use natCMF\core\App;
?>
<div class="col-xs-6 col-sm-3 col-sm-pull-9 sidebar-offcanvas" id="sidebar">
    <div class="list-group">
        <?php foreach ($menuItems as $item): ?>
            <a class="list-group-item<?= $item['pageId'] == $pageId ? ' active' : '' ?>" href="<?= App::href($item['action']) ?>"><?= $item['title'] ?></a>
        <?php endforeach; ?>
    </div>
</div><!--/.sidebar-offcanvas-->