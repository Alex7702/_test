<?php
use natCMF\core\Config;
use natCMF\core\App;
use natCMF\core\helpers\Html;

?>

<script>
    $(function(){
        $('.drop-column').on('click', function(){
            return confirm("Удалить столбец?");
        });
        $('.remove-row').on('click', function(){
            return confirm("Удалить строку?");
        });
    });
</script>

<?= $this->layout('navbar') ?>
<div class="container-fluid">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-md-12">
            <h2>админка</h2>
            <div class="content">
                
                <?= Html::table($table) ?>

            </div><!--/content-->
        </div><!--/.col-xs-12.col-sm-9-->
    </div><!--/row-->
</div><!--/.container-->

