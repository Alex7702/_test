<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\models;

use natCMF\core\SmartModel;


/**
 * Description of DbSession
 *
 * @author 27087
 */
class DbSession extends SmartModel {

    public function getCountByUserId($userId) {
        $result = $this->getRow('SELECT COUNT(*) AS count
            FROM {db_prefix}session
            WHERE user_id={int:user_id}', ['user_id' => $userId]);
        return $result['count'];
    }

    public function update($userId, $hash) {
        $this->query('UPDATE {db_prefix}session
            SET 
                last_update={int:last_update},
                user_agent_hash={string:user_agent_hash}
            WHERE user_id={int:user_id}
            LIMIT 1', [
            'last_update' => time(),
            'user_id' => $userId,
            'user_agent_hash' => $hash,
        ]);
    }

    public function add($userId, $hash) {
        $this->insertRow('{db_prefix}session', [
            'user_id' => ['int' => $userId],
            'last_update' => ['int' => time()],
            'user_agent_hash' => ['string' => $hash],
        ]);
    }

    public function getByHash($userAgentHash) {
        return $this->getRow('SELECT
                ses.user_id,
                ses.user_agent_hash,
                ses.last_update
                FROM {db_prefix}session AS ses
            WHERE ses.user_agent_hash={string:user_agent_hash}', [
                    'user_agent_hash' => $userAgentHash
        ]);
    }

    public function getByUserId($userId) {
        return $this->getRow('SELECT
                ses.user_id,
                ses.user_agent_hash,
                ses.last_update
                FROM {db_prefix}session AS ses
            WHERE ses.user_id={int:user_id}', [
                    'user_id' => $userId
        ]);
    }

    public function delete($userId) {
        $this->dbQuery('DELETE FROM {db_prefix}session 
            WHERE user_id={int:user_id}', [
            'user_id' => $userId,
        ]);
    }

    public function emptyOld() {
        $this->dbQuery('DELETE FROM {db_prefix}session 
            WHERE last_update < {int:expiried}', [
            'expiried' => time() - Config::get('sessionTime'),
        ]);
    }

}
