<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\models;

use natCMF\core\Config;
use natCMF\core\App;
use natCMF\core\ModelBase;
use natCMF\core\Session;
use natCMF\models\DbSession;
use natCMF\core\Cookie;

/**
 * Description of Auth
 *
 * @author 27087
 */
class Auth {

    /**
     *
     * @var object User
     */
    protected static $user = null;

    /**
     * Получаем текущее состояние пользователя
     * @return type
     */
    static function getInstance() {

        //Проверили есть ли уже такой экземпляр 
        if (self::$user !== null) {
            return true;
        }

        // Если в сессии уже есть ID пользователя то сразу создаем объект 
        if (!empty(Session::get('user_id'))) {
            self::$user = new User(Session::get('user_id'));
            return true;
        } else {
            self::$user = new User(0);
        }

        /*
         * Дальше у нас в $this->user - гостя
         * и мы пытаемся найти возможность узнать его
         */

        // Если в сессии нет уже данных то достаем из кук.. и если его там нет
        // тогда уходим, гость 
        $cookieUserAgentHash = (string) Cookie::get('secret');
        $cookieUserId = (string) Cookie::get('user_id');
        if (empty($cookieUserAgentHash) || empty($cookieUserId)) {
            return false;
        }

        //Проверили совпадение хеша из куки и сгенерированного хеша
        if ($cookieUserAgentHash != $this->generateUserAgentHash($cookieUserId)) {
            return false;
        }


        //Начинаем проверять через сессии в базе данных
        $dbSession = new DbSession;

        // Если в сессии баз данных тоже нет то в любом случае гость 
        $dbSessionData = $dbSession->getByHash($cookieUserAgentHash);
        if (empty($dbSessionData)) {
            return false;
        }

        // Если это хеш не пользователя из кук тогда тоже гость 
        if ($dbSessionData['user_id'] != $cookieUserId) {
            return false;
        }

        /* Больше не считаем пользователя гостем */
        Session::set('user_id', $dbSessionData['user_id']);
        self::$user = new User($dbSessionData['user_id']);

        return true;
    }

    static function set($userId) {

        //У гостя не может быть сессии
        if (empty($userId)) {
            return false;
        }

        //Создали сессию в БД и проверили наличие записи $userId
        $dbSession = new DbSession;
        $isExistDbSession = $dbSession->getCountByUserId($userId);

        //Собрали хэш
        $hash = $this->generateUserAgentHash($userId);

        //Добавили новую запись или обновили старую 
        if (empty($isExistDbSession)) {
            $dbSession->add($userId, $hash);
        } else {
            $dbSession->update($userId, $hash);
        }

        Cookie::set('secret', $hash);
        Cookie::set('user_id', $userId);
    }

    /**
     * Собираем хэщ
     * @param type $userId
     * @return type
     */
    private function generateUserAgentHash($userId) {
        $userAgent = (string) filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
        return sha1(Config::get('hashSalt') . $userAgent . $userId);
    }

    /**
     * Достаем из текущего объекта User 
     * свойство isAdmin
     * @return string
     */
    static function isAdmin() {
        return self::$user->isAdmin;
    }

    /**
     * Достаем из текущего объекта User 
     * свойство isGuest
     * @return string
     */
    static function isGuest() {
        return self::$user->isGuest;
    }

    /**
     * Достаем из текущего объекта User 
     * свойство isLogged
     * @return string
     */
    static function isLogged() {
        return self::$user->isLogged;
    }

    /**
     * Достаем из текущего объекта User 
     * свойство id
     * @return string
     */
    static function userId() {
        return self::$user->id;
    }

    /**
     * Достаем из текущего объекта User 
     * свойство email
     * @return string
     */
    static function userEMail() {
        return self::$user->eMail;
    }

    /**
     * Достаем из текущего объекта User 
     * свойство name
     * @return string
     */
    static function userName() {
        return self::$user->name;
    }



}
