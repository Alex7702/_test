<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core;

use natCMF\core\Db;
use natCMF\core\App;

/**
 * Description of SmartModel
 *
 * @author 27087
 */
abstract class SmartModel {

    public $tableName = null;
    public $tableCollumns = [];

    /**
     * 
     */
    public function __construct() {
        $this->tableCollumns = Db::tableInfo($this->tableName);
    }

    /**
     * Достать одну строку из таблицы
     * @param string $query
     * @param array $values
     * @return mixed
     */
    public function getRow( $query,  $values = []) {
        $result = Db::query($query, $values);

        $row = [];

        if (Db::numRows($result) > 0) {
            $row = Db::fetchAssoc($result);
        }
        Db::freeResult($result);

        return $row;
    }

    /**
     * Найти несколько строк - LIMIT указываем в $query
     * @param string $query
     * @param array $values
     * @return mixed
     */
    public function getRows($query, $values = []) {
        $result = Db::query($query, $values);

        $rows = [];
        if (Db::numRows($result) > 0) {
            while ($row = Db::fetchAssoc($result)) {
                $rows[] = $row;
            }
        }
        Db::freeResult($result);

        return $rows;
    }

    /**
     * 
     * @param string $query
     * @param array $values
     * @return int
     */
    public function getCount(string $query, array $values = []) {
        $result = Db::query($query, $values);
        list($count) = Db::fetchRow($result);
        Db::freeResult($result);
        return $count;
    }

    /**
     * Добавить что то в таблицу
     * @param string $table
     * @param array $data
     * @return type
     * <pre>
     *   INSERT INTO `smk_user_site` (`id`, `name`, `user_id`, `blank_id`, `params`) 
     *   VALUES(NULL, 'Мой 2', '1', '1', '');        
     *   $data = [
     *       'name' => 'Новый сайт',
     *       'user_id' => 1,
     *       'blank_id' => 1,
     *   ];
     * </pre>
     */
    public function insertRow($data, $inTable = '') {

        $table = empty($inTable) ? $this->tableName : $inTable;
        
        if (empty($table)) {
            die('Пустая таблица');
        }

        $query = '';
        $query .= 'INSERT INTO {db_prefix}' . $table . ' (';
        $columnsQuery = '';
        $valuesQuery = '';
        $values = [];
        foreach ($data as $name => $value) {
            $columnsQuery .= $name . ',';
            $valuesQuery .= '{' . $this->tableCollumns[$name]['type'] . ':' . $name . '},';
            $values[$name] = $value;
        }
        $query .= trim($columnsQuery, ',') . ') VALUES(' . trim($valuesQuery, ',') . ')';

        $result = Db::query($query, $values);

        $insertId = $result ? $result->insert_id : $result;

        return $insertId;
    }

    /**
     * Обновить данные в строке с id 
     * @param int $id
     * @param array $param
     * <code>
     *   $param = [
     *       'name' => 'Новый сайт',
     *       'user_id' => 1,
     *       'blank_id' => 1,
     *   ];
     * </code>
     * @return mixed
     */
    public function updateById(int $id, array $param) {

        $query = '';
        $query .= 'UPDATE {db_prefix}' . $this->tableName . ' SET';
        $setQuery = '';
        $values = [];

        foreach ($param as $name => $value) {
            $setQuery .= ' ' . $name . ' = {' . $this->tableCollumns[$name]['type'] . ':' . $name . '},';
            $values[$name] = $value;
        }
        $query .= trim($setQuery, ',') . ' WHERE id = {int:id}';
        $values['id'] = $id;

        return Db::query($query, $values);
    }

    /**
     * Удалить запись ID
     * @param int $id
     * @return mixed
     */
    public function deleteById($id) {
        return Db::query('DELETE FROM {db_prefix}' . $this->tableName . ' 
            WHERE id = {int:id}', [
                    'id' => $id,
        ]);
    }

}
