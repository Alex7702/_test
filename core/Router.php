<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core;

use natCMF\core\App;
use natCMF\core\Config;

/**
 * Простой роутинг
 * 
 *  Файл .htaccess
 *      RewriteEngine on 
 *      RewriteCond %{REQUEST_FILENAME} !-f
 *      RewriteCond %{REQUEST_FILENAME} !-d
 *      RewriteRule ^(.*)$ index.php?route=$1 [L,QSA]
 * 
 *  Теперь если в адресной строке у нас вот такая конструкция
 *  http://test.ru/front/site/page/34
 *  То: 
 *      front - это модуль
 *      site - это контроллер
 *      page - это метод экшена в контроллере
 *      34 - это параметр передаваемый в метод экшена
 * 
 * Если строка выглядит так 
 *  http://test.ru/page/34
 *  То роутер проверяет наличие контроллера PageController и если его нет 
 * считает что это экшен контролера по умолчанию 
 * таким образом мы пряем контроллер по умолчанию из строки
 * 
 * ВНИМАНИЕ: в методе parseURI и __construct стоят редиректы 
 * 1. Для того что бы убирать дефолтные значения из роута
 * 2. Для того что бы убирать слеш в конце роута
 * TODO: Заменить на правила в htaccess
 * 
 * @author я
 */
class Router {

    /**
     * Текущий путь, даже если в адресной строке путь не указан 
     * то все равно этот массив заполняется значениями по умолчанию
     * @var array 
     */
    protected static $route = [];
    protected static $callString = '';

    /**
     *
     * @var type 
     */
    protected static $uri = [];

    /**
     * Дефолтный модуль
     * он же дефолтный контроллер
     * дефолтный индекс всегда index
     * @var string 
     */
    protected $defaultModule = '';

    /**
     * Имя модуля который сейчас используется
     * @var type 
     */
    protected $module = '';

    /**
     * Имя класса который вызывается в результате текушего роута
     * @var string 
     */
    protected $classController = '';

    /**
     * Имя метода экшена который вызывается в результате текущего роута
     * @var string 
     */
    protected $methodAction = '';

    /**
     * Набор параметров который должен принять текущий экшен
     * @var array 
     */
    protected $methodParams = [];

    /**
     * Конструктор
     * парсим URI
     */
    public function __construct($defaultModule) {
        //Ставим наш дефолтный модуль
        $this->defaultModule = $defaultModule;

        /* Парсим URI */
        $this->parseURI();


        /* Фиксируем контроллер, экшен и параметры */
        $this->fixModule();
        $this->fixController();
        $this->fixAction();
        $this->fixParams();

        //Удаляем дефолтные пути, с учетом методов-фиксов выше
        $checkURI = $this->checkUriAndRedirect(self::$uri);

        //Если после чистки URI другой - тогда редиректим куда надо
        //запросы с POST не учитываются 
        //TODO так же надо игнорить запросы AJAX
        if ($checkURI != self::$uri) {
            $checkURI = empty($checkURI) ? '/' : $checkURI;
            App::redirect($checkURI);
        }
    }

    /**
     * Парсим URI 
     */
    private function parseURI() {

        /* Получили строку из $_GET['route'] параметра */
        self::$uri = trim((string) filter_input(INPUT_GET, 'route'), '/');

        /* Разбили строку и обрезали по бокам слеши */
        $tmpRoute = explode('/', self::$uri);

        self::$route = array_filter($tmpRoute, function($element) {
            return !empty($element);
        });
    }

    private function fixModule() {


        if (empty(self::$route['0'])) {
            /* Если пусто после указания компонента то берем контроллер по умолчанию */
            self::$route['0'] = $this->defaultModule;
        } else {

            /* Если что то указано в первом параметре то проверяем наличие такого модуля */
            if (!file_exists(Config::get('siteDir') . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR . self::$route['0'])) {
                /* Если нет такого метода то вставляем в начало массива контроллер по умолчанию */
                array_unshift(self::$route, $this->defaultModule);
            }
        }
        $this->module = self::$route['0'];
        return true;
    }

    /**
     * Фиксируем контроллер который надо использовать в роуте
     * и проверяем наличие такого контроллера, если такого контроллера нет
     * то подставляем контроллер по умолчанию
     * Таким образом мы скрываем из строки контроллер по умолчанию
     * @return boolean
     */
    private function fixController() {

        if (empty(self::$route['1'])) {
            /* Если пусто после указания компонента то берем контроллер по умолчанию */
            self::$route['1'] = self::$route['0'];
        } else {

            /* Если что то указано в первом параметре то проверяем наличие такого контроллера */
            if (!class_exists('\natCMF\app\controllers\\' . $this->module . '\\' . $this->parseRouteString(self::$route['1']) . 'Controller')) {
                /* Если нет такого метода то вставляем вторым номером контроллер по умолчанию для этого 
                 * модуля - он же равен названию модуля */
                array_splice(self::$route, 1, 0, $this->module);
            }
        }
        $this->classController = '\natCMF\app\controllers\\' . $this->module . '\\' . $this->parseRouteString(self::$route['1']) . 'Controller';


        return true;
    }

    /**
     * Фиксируем экшен который надо использовать в роуте, 
     * @return boolean
     */
    private function fixAction() {
        if (empty(self::$route['2'])) {
            /* Если нет экшена то берем экшен по умолчанию */
            self::$route['2'] = 'index';
        } else {
            /* Если экшен есть то проверяем его существования иначе отдает 404 экшен */
            if (!method_exists($this->classController, 'action' . $this->parseRouteString(self::$route['2']))) {
                self::$route['2'] = '404';
            }
        }
        $this->methodAction = 'action' . $this->parseRouteString(self::$route['2']);



        return true;
    }

    /**
     * Фиксируем параметры которые надо использовать в роуте
     * @return boolean
     */
    private function fixParams() {
        if (empty(self::$route['3'])) {
            self::$route['3'] = [];
        } else {
            /* Пересобираем route выделяя параметры в отдельный массив */
            $params = self::$route;
            self::$route = [];
            self::$route['0'] = $params['0'];
            self::$route['1'] = $params['1'];
            self::$route['2'] = $params['2'];
            unset($params['0']);
            unset($params['1']);
            unset($params['2']);
            self::$route['3'] = array_values($params);
        }



        /* Для метода проверяем количество принимаемых параметров */
        $classMethod = new \ReflectionMethod($this->classController, $this->methodAction);

        /* Отдаем ровно столько сколько нужно отдать.. */
        $this->methodParams = array_slice(self::$route['3'], 0, count($classMethod->getParameters()));
        return true;
    }

    /**
     * Обрабатываем строку типа page-info что бы получить PageInfo
     * @param string $inStr
     * @return string
     */
    private function parseRouteString($inStr) {
        $result = '';
        $names = explode('-', $inStr);
        foreach ($names as $name) {
            $result .= ucfirst($name);
        }
        return $result;
    }

    /* Поехали (с) */

    public function run() {

        self::$callString = $this->classController . '\\' . $this->methodAction;

        // Создаем объект контроллера
        $controller = new $this->classController;

        // Вызываем метод объекта
        call_user_func_array(array($controller, $this->methodAction), $this->methodParams);
    }

    /**
     * 
     * @return type
     */
    static function uri() {
        return self::$uri;
    }

    /**
     * Достать текущий роут
     * @param type $key
     * @return type
     */
    static function getRoute($key = false) {
        if ($key === false) {
            return self::$route;
        } else {
            return self::$route[$key];
        }
    }

    static function getCall() {
        return self::$callString;
    }

    /**
     * Очищаем строку от дефолтных значений
     * @return string
     */
    protected function checkUriAndRedirect($uri) {

        /* Разбили строку и обрезали по бокам слеши */
        $tmpRoute = explode('/', trim($uri, '/'));
        $tmpRoute = empty($tmpRoute) ? [] : $tmpRoute;
        $origRoute = array_filter($tmpRoute, function($element) {
            return !empty($element);
        });

        //Если есть POST запрос то ничего не трогать)
        $post = filter_input_array(INPUT_POST);
        if (App::isAjax() || !empty($post)) {
            return $uri;
        }
        
        //Создали копию роута с которой работать будем
        $draftRoute = $origRoute;
        
        //Проверили есть ли в конце index, если есть - убрали
        $reversRoute = array_reverse($origRoute);
        if (
                isset($reversRoute[0]) &&
                $reversRoute[0] == 'index'
        ) {
            $reversRoute[0] = '';
            $draftRoute = array_reverse($reversRoute);
        }

        //Убрали индекс когда есть и модуль и контроллер и нет параметров
        if (
                isset($draftRoute[2]) &&
                !isset($draftRoute[3]) &&
                self::$route[2] == $draftRoute[2] &&
                $draftRoute[2] == 'index'
        ) {

            $draftRoute[2] = '';
        }

        //Убрали контроллер по умолчанию
        if (
                isset($draftRoute[1]) &&
                self::$route[1] == $draftRoute[1] &&
                $draftRoute[0] == $draftRoute[1]
        ) {
            $draftRoute[1] = '';
        }

        //Убрали модуль по умолчанию
        if (
                isset($draftRoute[0]) &&
                self::$route[0] == $draftRoute[0] &&
                $draftRoute[0] == $this->defaultModule
        ) {
            $draftRoute[0] = '';
        }

        //Убрали пустые элементы
        $newRoute = array_filter($draftRoute, function($element) {
            return ($element != '');
        });

        return empty($newRoute) ? '' : implode('/', $newRoute);
    }

    

    
}
