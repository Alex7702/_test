<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core;

use natCMF\core\SessionBase;

/**
 * Description of SessionNative
 *
 * @author 27087
 */
class SessionNative implements SessionBase {

    /**
     * Берем значение из сессии
     * @param type $key
     * @return mixed
     */
    public function get($key) {
        $value = isset($_SESSION[$key]) ? $_SESSION[$key] : null;
        return $value;
    }

    /**
     * Устанавливаем значение элемента сессии
     * @param type $key 
     * @param type $value
     * @return void
     */
    public function set($key, $value) {
        $_SESSION[$key] = $value;
    }

    /**
     * Стартуем сессию
     * @return boolean
     */
    public function start() {
        return session_start();
    }

    /**
     * Удаляем из сессии
     * @param string $key
     */
    public function delete($key) {
        unset($_SESSION[$key]);
    }

}
