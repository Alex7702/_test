<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core\helpers;

/**
 * Description of Grid
 *
 * @author 27087
 */
class Html {

    /**
     * Строим табличку
     * @param array $table - массив с данными 
     * <pre>
     * [
     *   'data' => $data,                   // массив с данными 
     *   'id' => 'myTable',                 // id таблицы
     *   'title' => 'Моя таблица',          // заголовок таблицы - если нет то его не буде)
     *   'columns' => [                     // массив с колонками
     *       'id' => [                      // "id" - в данном случае совпадает 
     *                                      // с ключем значения из массива $data
     *           'title' => 'ИД',           // заголовок таблицы th (thead)
     *           'thClass' => '',           // класс для th (задаем ширину таблицы) 
     *                                      // С помощью twitter bootstrap 3 используйте: 
     *                                      // class="col-md-*" где * - количество столбцов ширины.
     *                                      //  tr class="something"<>
     *                                      //     td class="col-md-2" A /td  
     *                                      //     td class="col-md-3" B /td  
     *                                      //     td class="col-md-6" C /td  
     *                                      //     td class="col-md-1" D /td  
     *                                      //  tr
     * 
     *           'tdClass' => '',           // класс для td ячейки - если нужно  
     *                                      //    выделить столбец одним цветом
     * 
     *           'value' => function($v){   // значение которое будет вставленно
     *               $s = '';               //    в ячейку. В функцию передается
     *               $s .= $v['id'];        //    значение ВСЕЙ строки таблицы 
     *               return $s;             //    (один массив из $data)
     *           },
     *       ],
     *       'param1' => [                  // описание следующего столбца
     *           'title' => 'Параметр 1',
     *           'value' => function($v){
     *               $s = '';
     *               $s .= $v['param1'];
     *               return $s;
     *           },
     *       ],
     *   ],
     * ]
     * Чередование разных цветов tr надо делать с помощью CSS
     *   table tbody tr:nth-child(odd){     // Задаем стили для нечетных рядов таблицы
     *           background: #fff;
     *   }
     *   table tbody tr:nth-child(even){    // Задаем стили для четных рядов таблицы
     *           background: #f2f6f8;
     *   }
     * </pre>
     * 
     */
    static function table($table) {
        
        $string = '';
        
        $string .= '
        <div class="panel panel-default">
            <!-- Default panel contents -->';

        // Если титл объявлен то отображаем его 
        if (isset($table['title'])) {
            $string .= '
            <div class="panel-heading">' . $table['title'] . '</div>';
        }

        // Если объявлен id таблицы то добавляем его
        $id = isset($data['id']) ? ' id="' . $data['id'] . '"' : '';
        $string .= '
            <!-- Table -->
            <table class="table"' . $id . '>
                <thead>
                    <tr>';

        // Проходим все колонки таблицы и строки заголовок
        foreach ($table['columns'] as $v) {
            $thClass = empty($v['thClass']) ? '' : ' class="' . $v['thClass'] . '"';
            $string .= '
                        <th' . $thClass . '>' . $v['title'] . '</th>';
        }

        $string .= '
                    </tr>
                </thead>
                <tbody>';

        // Начинаем выводить данные 
        foreach ($table['data'] as $v1) {
            $string .= '
                    <tr>';
            // Опять проходим массив с колонками, строя нужное количество колонок 
            foreach ($table['columns'] as $k2 => $v2) {
                $tdClass = empty($v['tdClass']) ? '' : ' class="' . $v['tdClass'] . '"';
                $tdVal = '';
                
                // Данные для ячейки, если там анонимная функция - то выполняем ее
                if (is_object($v2['value'])) {
                    $tdVal = $v2['value']($v1);
                } else {
                    $tdVal = $v1[$k2];
                }
                $string .= '
                        <td' . $tdClass . '>' . $tdVal . '</td>';
            }
            $string .= '
                    </tr>';
        }
        $string .= '
                </tbody>
            </table>
        </div>';
        return $string;
    }

    
    /**
     * Готовим строку для вывода.
     * @param string $string
     * @return string
     */
    static function preparetext(string $string) {
        $result = (string) htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
        return $result;
    }
    
    
    

}
