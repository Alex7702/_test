<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core;

use natCMF\core\Config;

/**
 * Description of View
 *
 * @author 27087
 */
class View {
    
    const START_OF_PAGE = 1;
    const END_OF_PAGE = 2;

    private $data;
    private $action;
    private $templateName;
    private $templateUrl;
    
    private $jsCode;
    private $cssCode;

    public function __construct($templateName) {
        $this->templateName = $templateName;
        $this->templateUrl = Config::get('siteUrl') . '/app/views/' . $templateName;
    }

    public function render($action, $data = []) {
        
        
        $this->data = $data;
        $this->action = $action;
        extract($this->data);
        
        ob_start();
        include(Config::get('siteDir') . '/app/views/' . $this->templateName . '/layouts/main.php');
        $string = ob_get_clean();
        
        echo $string;
        return '';
    }

    private function layout($layout) {
        extract($this->data);
        include Config::get('siteDir') . '/app/views/' . $this->templateName . '/layouts/' . $layout . '.php';
        return '';
    }

    private function action($action) {
        extract($this->data);
        include Config::get('siteDir') . '/app/views/' . $this->templateName . '/' . $action . '.php';
        return '';
    }
    
    private function addJs($code, $position = 1) {
        echo $code;
    }
    

}
