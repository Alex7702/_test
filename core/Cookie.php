<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core;

/**
 * Description of Cookie
 *
 * @author 27087
 */
class Cookie {

    /**
     * Устанавливаем куку
     * @param type $key
     * @param type $value
     * @param type $time
     * @return type
     */
    public static function set($key, $value, $time = 31536000) {
        return setcookie($key, $value, time() + $time, '/');
    }

    /**
     * Достаем куку
     * @param type $key
     * @return type
     */
    public static function get($key) {
        $value = filter_input(INPUT_COOKIE, $key);
        return $value;
    }

    /**
     * Удаляем куку
     * @param type $key
     */
    public static function delete($key) {
        $value = filter_input(INPUT_COOKIE, $key);
        if ($value) {
            self::set($key, '', -3600);
            unset($_COOKIE[$key]);
        }
    }

}
