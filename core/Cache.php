<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core;

/**
 * Description of Cache
 *
 * @author 27087
 */
class Cache {
    
    /**
     * 
     * @param string $key - имя элемента кэша
     * @param mixed $value - то что нужно положить в кэш
     * @param string $time - время жизни кэша в секундах
     * @return boolean - true в случае успеха, иначе false
     */
    static function set(string $key, $value, $time = 120){
        
        return true;
    }
    
    /**
     * Достать что то из кеша
     * @param string $key - имя элемента в кэше
     * @return mixed - false в случае если нет такого элемента, иначе 
     * смешанный тип. Резуальтат уже готовый. Не нужно ничего 
     * десерилализовать и т.п. 
     */
    static function get(string $key) {
        
        return $result;
    }
    
    /**
     * Есть ли элемент в кеше
     * @param string $key - имя элемента
     * @return boolean - результат
     */
    static function exist($key){
        
        return true;
    }
    
    /**
     * Удалить элемент из кэша
     * @param string $key
     * @return boolean - можно и ничего не возвращать, если такого элемента 
     * нет, то удалять нечего, если элемент есть то удалит
     */
    static function purge($key){
        
        return true;
    }
    
}
