<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core;

/**
 * Description of Config
 *
 * @author 27087
 */
class Config {

    static private $_instance = null;
    private $_registry = array();

    static public function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    static public function set($key, $object) {
        self::getInstance()->_registry[$key] = $object;
    }

    static public function get($key) {
        return isset(self::getInstance()->_registry[$key]) ? self::getInstance()->_registry[$key] : null;
    }
    
    static public function view() {
        var_dump(self::getInstance()->_registry);
    }

    private function __wakeup() {
        
    }

    private function __construct() {
        
    }

    private function __clone() {
        
    }

}
