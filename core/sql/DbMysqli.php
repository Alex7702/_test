<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core\sql;

use natCMF\core\sql\DbInterface;

/**
 * Description of DbMysqli
 *
 * @author 27087
 */
class DbMysqli implements DbInterface {

    private $db = null;
    private $dbPrefix = null;
    private $colTypeLink = [
        'varchar' => 'string',
        'text' => 'string',
        'int' => 'int',
        'float' => 'float',
    ];
    private $_count = 0;

    public function __construct($dbParams) {
        $this->dbPrefix = $dbParams['dbPrefix'];
//            mysqli_report(MYSQLI_REPORT_ALL);
//            mysqli_report(MYSQLI_REPORT_INDEX);
//            mysqli_report(MYSQLI_REPORT_STRICT);
        $db = new \mysqli($dbParams['dbHost'], $dbParams['dbUser'], $dbParams['dbPass'], $dbParams['dbName']);

        $db->set_charset('utf8mb4');
        $db->character_set_name();
        $db->query("SET CHARACTER SET utf8mb4");
        if (empty($db->errno)) {
            $this->db = $db;
        }
    }

    /**
     * 
     * @param string $query - запрос 
     * с плейсхолдерами int|float|string|array_int|array_string
     * id={int:id} AND id_member IN(array_ind:ids)
     * @param array $values массив с даными для плейсходеров
     * [ id=>1, ids=>[1,2,3,4,5] ]
     * @return object mysqli_result
     */
    public function query(string $query, array $values = []) {

        //Считаем запросы к БД
        $this->_count++;

        /* Типы плейсхолдеров */
        $pattern = '~{(int|float|string|array_int|array_string):(\S+?)}~';

        /* Выбираем из строки запрос возможные плейсходеры */
        $matches = [];
        preg_match_all($pattern, $query, $matches, PREG_SET_ORDER);

        /* Инициализируем массив с данными плейсхолтера */
        $prepareData = [];
        /* и массив с типами плейсхолдера mysqli 
         *   i - соответствующая переменная имеет тип integer
         *   d - соответствующая переменная имеет тип double
         *   s - соответствующая переменная имеет тип string
         *   b - соответствующая переменная является большим двоичным объектом (blob) и будет пересылаться пакетами
         */
        $typesPlaceholders = '';

        /* Обходим найденные плейсхолдеры */
        foreach ($matches as $ph) {
            /* Временный плейсхоллер нужен для типов плейсхолдеров array_XXX 
              знак вопроса ? для самого запроса */
            $tmpPlaceholers = '';

            switch ($ph['1']) {
                case 'int':
                    $typesPlaceholders .= 'i';
                    $prepareData[] = (int) $values[$ph['2']];
                    $tmpPlaceholers = '?';
                    break;

                case 'array_int':
                    $tmpPlaceholers = '';
                    foreach ($values[$ph['2']] as $intVal) {
                        $typesPlaceholders .= 'i';
                        $prepareData[] = (int) $intVal;
                        $tmpPlaceholers .= '?,';
                    }
                    $tmpPlaceholers = trim($tmpPlaceholers, ',');
                    break;

                case 'string':
                    $typesPlaceholders .= 's';
                    $prepareData[] = (string) $values[$ph['2']];
                    $tmpPlaceholers = '?';
                    break;

                case 'array_string':
                    break;

                default:

                    die('Неверный тип даннных ' . $ph['1']);
                    break;
            }
            /* Заменили плейсхолдер на нужное количество знаков вопросов */
            $query = str_replace($ph['0'], $tmpPlaceholers, $query);
        }
        /* Заменили плейсхолдер префикса таблицы на сам префикс */
        $query = str_replace('{db_prefix}', $this->dbPrefix, $query);
        /* Собрали массив для вызова метода */
        $params['db_types'] = $typesPlaceholders;
        foreach ($prepareData as $key => $val) {
            $params['db_' . $key] = &$prepareData[$key];
        }



        /* Готовим запрос и выполняем */
        $stmt = $this->db->stmt_init();
        $stmt->prepare($query);

        if ($stmt->error) {
            var_dump($query);
            var_dump($stmt->error);
        }



        if (!empty($params['db_types'])) {
            call_user_func_array([$stmt, 'bind_param'], $params);
        }

        $stmt->execute();

        //Если INSERT то возвращаем то что показал stmp
        if (!empty($stmt->insert_id)) {
            $result = new \stdClass();
            foreach ($stmt as $key => $value) {
                $result->$key = $stmt->$key;
            }
        }
        //Если запрос был SELECT, UPDATE, DELETE - тогда берем результат
        else {
            $result = $stmt->get_result();
        }
        $stmt->close();
        return $result;
    }

    /**
     * Количество записей в результате запроса 
     * @param object \mysqli_result $result
     * @return int
     */
    public function numRows($result) {
        return $result->num_rows;
    }

    /**
     * Очищаем mysqli_result
     * @param $result - object \mysqli_result 
     * @return boolean
     */
    public function freeResult(&$result) {
        return $result->free_result();
    }

    /**
     * Получаем ассоциативный массив с mysqli_result и передвигаем указатель на 
     * следующий элемент
     * @param $result - object \mysqli_result 
     * @return array - массив с результаом
     */
    public function fetchAssoc($result) {
        return $result->fetch_assoc();
    }

    /**
     * 
     * @param $result - object \mysqli_result 
     * @return type
     */
    public function fetchRow($result) {
        return $result->fetch_row();
    }

    /**
     * 
     * @param type $table
     * @return type
     */
    public function tableInfo($table) {

        $result = $this->query('SHOW COLUMNS FROM {db_prefix}' . $table, []);

        $rows = [];
        if ($this->numRows($result) > 0) {
            while ($row = $this->fetchAssoc($result)) {
                $rows[] = $row;
            }
        }
        $this->freeResult($result);

        return $this->tableInfoAdaptation($rows);
    }

    /**
     * 
     * @param type $result
     * @return type
     */
    private function tableInfoAdaptation($result) {
        $linksType = [];
        $matches = [];

        foreach ($result as $coll) {
            preg_match('~^([a-z]{1,})\(?([\d]{0,})\)?~', $coll['Type'], $matches);
            $type = $matches[1];
            if (isset($this->colTypeLink[$type])) {
                $linksType[$coll['Field']] = [
                    'type' => $this->colTypeLink[$type],
                    'lenght' => $matches[2],
                ];
            } else {
                die('Ошибка, нет типа столбца: ' . __FILE__ . ' - ' . __LINE__);
            }
        }
        return $linksType;
    }

    /**
     * Возвращаем количество запросов к БД
     * @return int
     */
    public function getCountQuery() {
        return $this->_count;
    }

}
