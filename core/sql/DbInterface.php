<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core\sql;

/**
 * Description of DbInterface
 *
 * @author 27087
 */
interface DbInterface {
    
    public function __construct($dbParams);
    
    public function query(string $query, array $values = []);
    
    public function numRows($result);
    
    public function freeResult(&$result);
    
    public function fetchAssoc($result);
    
    public function fetchRow($result);
    
    public function tableInfo($table);
    
    public function getCountQuery();
    
}
