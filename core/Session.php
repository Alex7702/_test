<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace natCMF\core;

/**
 * Description of Session
 *
 * @author 27087
 */
class Session {

    /**
     * Объект текущего типа сессии
     * @var array 
     */
    protected static $session = null;

    /**
     * Стартуем сессии
     * @param string $type
     *      'native' - нативные ПХП сессии, 
     * @return boolean
     *      true - успешно созданная сессия
     *      false - неуспешно))
     */
    public static function start($type = 'native') {
        if (self::$session !== null) {
            return true;
        }
        
        switch ($type) {
            case 'native':
                self::$session = new \natCMF\core\SessionNative;
                break;

            default:
                self::$session = new \natCMF\core\SessionNative;
                break;
        }
        return self::$session->start();
    }

    /**
     * Достаем значение сессии
     * @param string $key - имя ключа в сессии
     * @return mixed - значенеи из сессии
     */
    public static function get(string $key) {
        $value = self::$session->get($key) ?? null;
        return $value;
    }

    /**
     * Устанавливаем значение сессии
     * @param type $key
     * @param type $value
     */
    public static function set(string $key, $value) {
        self::$session->set($key, $value);
    }
    
    /**
     * Удаляем из сессии
     * @param string $key
     */
    public static function delete(string $key) {
        self::$session->delete($key);
    }

}
